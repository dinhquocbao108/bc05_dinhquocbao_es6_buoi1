export function renderColorList(colorList){
    let content="";    
    colorList.forEach((color)=>{
        content +=`<button id="${color}" onclick="getColor(this.id)" class="color-button ${color}"></button>`;
        return content;
    });
    document.getElementById("colorContainer").innerHTML = content;
}

