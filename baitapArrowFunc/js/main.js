const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];
import {renderColorList} from "./controller.js";
renderColorList(colorList);
document.getElementById("pallet").classList.add("active");
function getColor(color){
    document.getElementById("house").classList=(`house ${color}`);
    changeActiveColor(color);
}
function changeActiveColor(colorActive){
    colorList.forEach((color)=>{
        if(color===colorActive){
            document.querySelector(`#${color}`).classList.add("active");
        }else{
            document.querySelector(`#${color}`).classList.remove("active");
        }
    });
}

window.getColor = getColor;


