import { layThongTinInput,tinhDTB } from "./controller.js";
function tinhKhoi1(){
    document.getElementById("btnKhoi1").addEventListener("click",()=>{
        let num = layThongTinInput();
        document.getElementById("tbKhoi1").innerHTML = tinhDTB(num.ly,num.toan,num.hoa).toFixed(2);
    })
};
function tinhKhoi2(){
    document.getElementById("btnKhoi2").addEventListener("click",()=>{
        let num = layThongTinInput();
        document.getElementById("tbKhoi2").innerHTML = tinhDTB(num.dia,num.su,num.van,num.english).toFixed(2);
    })
}
tinhKhoi1();
tinhKhoi2();
